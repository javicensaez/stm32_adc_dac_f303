/*
 * procesado.c
 *
 *  Created on: 5 nov. 2019
 *      Author: javivi
 */
#include "procesado.h"
#include "stm32f3xx_hal.h"
#include "arm_math.h"

arm_fir_instance_q15 S;

#define numeroEtapas 30
static q15_t firStateQ15[frameLength + numeroEtapas - 1];

static q15_t firCoeffs[numeroEtapas] = { 381, -441, -612, 12, 669, 113, -988,
		-574, 1226, 1347, -1444, -3001, 1584, 10265, 14749, 10265, 1584, -3001,
		-1444, 1347, 1226, -574, -988, 113, 669, 12, -612, -441, 381, 0 };

void init_procesado() {
	arm_fir_init_q15(&S, numeroEtapas, &firCoeffs[0], &firStateQ15[0],frameLength);
}

void procesar(q15_t *inData, q15_t *outData, int32_t length) {
		arm_fir_q15(&S, inData, outData, length);
		//passthrough(inData, outData, length);
}

void passthrough(q15_t *inData, q15_t *outData, int32_t length) {
	for (int n = 0; n < length; n++) {
		outData[n] = inData[n];
	}
}

